<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Ixudra\Curl\Facades\Curl;


class GoogleDriveController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:docx,csv,txt,xlx,xlsx,pdf,pptx'
            ]);
        if($request->file()) {
            $fileName = $request->file->getClientOriginalName();
            $content = file_get_contents($request->file('file'));
            Storage::disk('google')->put($fileName, $content);
            return back()->with('success','File has been uploaded');
        }
    }

    public function list()
    {
        $files = Storage::disk('google')->allFiles();
        $fileArray = [];
        foreach($files as $file)
        {
            $details = Storage::disk('google')->getMetadata($file);
            $fileArray[$details['name']] = $details;
        }
        return view('list',['files' => $fileArray]);
    }

    public function view($id)
    {
        return view('view',['id' => $id]);
    }

    public function drive($data = [])
    {
        $response = Curl::to('https://www.onedrive.com/embed?')
        ->withHeaders([
            'view'  =>true,
            'cid'   =>'CD04BF99C0FF654B',
            ])
        ->withResponseHeaders()
        ->returnResponseObject()
        ->get();
            $data = $response;
            // dd($response);
    return view('board.index', compact($response))->render();
    }

    public function onedrive(Request $request)
    {
        $url = "/drives/{cd813fa6-5328-437c-a9c8-8fc7bd56df23}/items/{itemId}/preview";

        $curl = curl_init();
//?d=w7408974c561e4c58bf4ec83a48e7e95c&csf=1&web=1&e=0Wzh3H
// string url = "https://api.onedrive.com/v1.0/drive/root:/" + url_path_encode(path) + ":/children")



// https://graph.microsoft.com/v1.0/848d7039-c8c4-4d4e-8b28-da7bd7a9efdb/drive/w7408974c561e4c58bf4ec83a48e7e95c/preview"
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://graph.microsoft.com/v1.0/848d7039-c8c4-4d4e-8b28-da7bd7a9efdb/drive/w7408974c561e4c58bf4ec83a48e7e95c/preview",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => [
            "Accept: application/json",
            // "Authorization: Bearer 7Az2xKBSEQccFc2ZAw-sIVEyveg",
            "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }
        else {
            $id = $response;
            dd($id);
            return view('onedrive',['id'=>$id]);
        }
    }

}
