<?php

use App\Http\Controllers\GoogleDriveController;
use App\Http\Controllers\WhiteboardController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('test', function() {
//     Storage::disk('google')->put('test.txt', 'Hello World');
//     dd("done");
// });

Route::get('upload', [GoogleDriveController::class,'index']);
Route::post('upload', [GoogleDriveController::class,'store']);
Route::get('list', [GoogleDriveController::class,'list']);
Route::get('view/{id}', [GoogleDriveController::class,'view']);

// onedrive
Route::get('onedrive', [GoogleDriveController::class,'onedrive']);


Route::get('board', [WhiteboardController::class,'index']);
Route::get('board/new', [WhiteboardController::class,'new']);

// Route::get('test', [WhiteboardController::class, 'test']);
