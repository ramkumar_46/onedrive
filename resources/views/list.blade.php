<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>Google Drive Files</title>
</head>
<body>
<center>
    <div class="container mt-5">
        <a href="/upload" class="btn btn-info" style="float: right">Upload new file</a><br><br><br>
        <table class="table">
            <tr class="table-warning">
                <td>Title</td>
                <td>Extension</td>
                <td>Action</td>
            </tr>
            @foreach ($files as $file)
            <tr>
                <td>{{ $file['filename'] }}</td>
                <td>
                    <label style="background: greenyellow;border-radius:5px;width:40px;text-align:center">
                        .{{ $file['extension'] }}
                    </label>
                </td>
                <td>
                    <a href="{{url('view/'.$file['path'])}}">View</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</center>
</body>
</html>
