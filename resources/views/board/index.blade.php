
<script language="JavaScript" type="text/javascript"
        src="https://kjur.github.io/jsrsasign/jsrsasign-latest-all-min.js">
</script>

<script type="text/javascript">
var oHeader = {alg: 'HS256', typ: 'JWT'};
// Payload
var oPayload = {};
var tNow = KJUR.jws.IntDate.get('now');
var tEnd = KJUR.jws.IntDate.get('now + 1day');
oPayload.iss = "3074457358102083181";
oPayload.nbf = tNow;
oPayload.iat = tNow;
oPayload.exp = tEnd;
// Sign JWT, password=616161
var sHeader = JSON.stringify(oHeader);
var sPayload = JSON.stringify(oPayload);
var sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, "oCQHoAZAGXv9Goyan30cYUV5KSY8swK8");
// var sJWT = KJUR.jws.JWS.sign("HS256", sHeader, sPayload, "nUcNRmWtZ6sDc2ceYpnYHrsPaFBxmbqd");
</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Boards picker demo</title>
    <style>
        html {
            font-family: Arial;
            font-size: 14px;
            background: #f6f6f6;
            padding: 10px;
        }
        button {
            font-family: Arial;
            display: inline-block;
            vertical-align: baseline;
            font-size: 14px;
            height: 32px;
            background-color: white;
            margin-left: 12px;
            border-radius: 4px;
            border: 1px solid #999;
        }
        .results {
            margin-top: 20px;
            white-space: pre;
            display: none;
            width: 100%;
            height: 200px;
            border: none;
            background-color: rgba(0,0,0,0);
            font-family: Arial;
            font-size: 12px;
            resize: none;
        }
    </style>
    <script src="https://miro.com/app/static/boardsPicker.1.0.js"></script>
</head>
<body>
<button>
    Open Board
</button>
<textarea class="results"></textarea>

<script>
    console.log(sJWT);
	var button = document.querySelector('button')
	var resultsBox = document.querySelector('.results')
	button.addEventListener('click', () => {
		miroBoardsPicker.open({
			clientId: '3074457358102083181', // Replace it with your app ClientId
			action: 'access-link',
            allowCreateAnonymousBoards: true,
            getAnonymousUserToken: ()   => Promise.resolve(sJWT),
			success: (data) => {
				resultsBox.innerHTML = JSON.stringify(data, null, 4)
				resultsBox.style.display = "block"
			},
      error: e => {
      console.log('on error : ', e)
      },
      cancel: () => {
        console.log('on cancel')
      },
		})
	});
</script>
{{-- <script>
    miroBoardsPicker.open({
        clientId: '3074457358102083181', // Replace it with your app ClientId
        action: 'access-link',
        allowCreateAnonymousBoards: true,
        getAnonymousUserToken: ()   => Promise.resolve(sJWT),
        success: (data) => {
            resultsBox.innerHTML = JSON.stringify(data, null, 4)
            resultsBox.style.display = "block"
        },
        error: e => {
        console.log('on error : ', e)
        },
        cancel: () => {
            console.log('on cancel')
        },
	})
</script> --}}
</body>
</html>

<iframe class="miro-embedded-board"
src="https://miro.com/app/live-embed/o9J_l6Z4n3A=?boardAccessToken=P4ONHwPNMilGrj5il68q7KUk0MvdlJ03&autoplay=true"
referrerpolicy="no-referrer-when-downgrade"
frameborder="0"
width="700" height="480"
style="background: transparent; border: 1px solid #ccc;">
</iframe>
